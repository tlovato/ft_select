/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ending_term.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/27 08:13:54 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/27 08:14:10 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_select.h"

int				escape(char read)
{
	if (read == 27)
		return (1);
	else
		return (0);
}

void			endterm(struct termios *term, struct termios *init, t_all *lst)
{
	term->c_lflag |= (ICANON);
	term->c_lflag |= (ECHO);
	tcsetattr(lst->fd, TCSANOW, init);
	tputs(tgetstr("ve", NULL), lst->fd, ft_puts);
}

void			free_all(t_all *lists)
{
	if (lists->choices)
		lists->choices = free_choices(lists->choices);
	if (lists->ret)
		lists->ret = free_ret(lists->ret);
	if (lists->tmpfd)
		free(lists->tmpfd);
}
