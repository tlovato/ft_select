/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   suppr_elem.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/03 12:02:32 by tlovato           #+#    #+#             */
/*   Updated: 2016/10/03 12:02:45 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_select.h"

static void			case_one(t_select **choices)
{
	(*choices)->cursor = 0;
	if ((*choices)->next)
	{
		(*choices)->next->cursor = 1;
		*choices = (*choices)->next;
	}
	else
		*choices = NULL;
}

static void			case_two(t_select **tmp, t_select **choices, t_select **cur)
{
	(*tmp)->cursor = 0;
	if ((*tmp)->next)
		(*tmp)->next->cursor = 1;
	else
		(*choices)->cursor = 1;
	(*cur)->next = (*tmp)->next;
}

static void			suppr_in_new(char *name, t_return **new)
{
	t_return		*tmp;
	t_return		*cur;

	tmp = *new;
	cur = *new;
	while (tmp && ft_strcmp(tmp->choice, name))
	{
		cur = tmp;
		tmp = tmp->next;
	}
	if (!tmp)
		return ;
	if (cur == tmp)
		*new = (*new)->next;
	else if (tmp)
		cur->next = tmp->next;
	free(tmp->choice);
	free(tmp);
	tmp = NULL;
}

t_return 			*suppr_elem(t_select **c, unsigned int read, t_return *new)
{
	t_select		*tmp;
	t_select		*cur;

	if (read == 127 || read == 2117294875)
	{
		tmp = *c;
		cur = *c;
		while (tmp && !(tmp->cursor))
		{
			cur = tmp;
			tmp = tmp->next;
		}
		if (cur == tmp)
			case_one(c);
		else if (tmp)
			case_two(&tmp, c, &cur);
		if (new)
			suppr_in_new(tmp->choice, &new);
		free(tmp->choice);
		free(tmp);
		tmp = NULL;
		tputs(tgetstr("cl", NULL), 0, ft_puts);
	}
	return (new);
}
