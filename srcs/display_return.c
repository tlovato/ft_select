/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_return.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/30 17:21:58 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/30 17:22:11 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_select.h"

void		display_return(t_return *ret)
{
	tputs(tgetstr("ho", NULL), 0, ft_puts);
	tputs(tgetstr("cl", NULL), 0, ft_puts);
	if (ret)
	{
		while (ret)
		{
			ft_putstr(ret->choice);
			if (ret->next)
				ft_putchar(' ');
			ret = ret->next;
		}
		ft_putchar('\n');
	}
}
