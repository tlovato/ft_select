/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_cursor.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/27 09:04:51 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/27 09:05:01 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_select.h"

static void			move_right(t_select *tmp)
{
	t_select		*start;

	start = tmp;
	while (tmp)
	{
		if (tmp->cursor)
		{
			tmp->cursor = 0;
			if (tmp->next)
				tmp->next->cursor = 1;
			else
				start->cursor = 1;
			return ;
		}
		if (!(tmp->next))
			move_right(start);
		else
			tmp = tmp->next;
	}
}

static t_select		*find_last(t_select *tmp)
{
	while (tmp->next)
		tmp = tmp->next;
	return (tmp);
}

static void			move_left(t_select *tmp)
{
	t_select		*last;
	t_select		*prev;
	int				cond;

	last = find_last(tmp);
	prev = tmp;
	if (prev->cursor)
	{
		prev->cursor = 0;
		last->cursor = 1;
	}
	else
		while (tmp)
		{
			prev = tmp;
			if (tmp->next && tmp->next->cursor)
			{
				tmp->next->cursor = 0;
				prev->cursor = 1;
				return ;
			}
			if (tmp->next)
				tmp = tmp->next;
		}
}

int					move_cursor(t_all *lists, unsigned int read)
{
	t_select		*tmp;

	tmp = lists->choices;
	if (read == 27)
		return (0);
	else if (read == 4414235)
		move_right(tmp);
	else if (read == 4479771)
		move_left(tmp);
	else if (read == 4348699)
		move_down(tmp, lists);
	else if (read == 4283163)
		move_up(tmp, lists);
	else if (read == 10)
	{
		lists->enter = 1;
		return (0);
	}
	return (1);
}
