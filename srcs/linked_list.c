/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   linked_list.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/21 10:13:23 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/21 10:13:32 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_select.h"

t_select			*get_list(char **av, int i, int cur)
{
	t_select		*new;

	if (av[i])
	{
		if ((new = (t_select *)malloc(sizeof(t_select))) == NULL)
			return (NULL);
		new->choice = ft_strdup(av[i]);
		new->padding = NULL;
		new->cursor = cur;
		new->select = 0;
		new->next = get_list(av, i + 1, 0);
		return (new);
	}
	return (NULL);
}
