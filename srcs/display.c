/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/20 12:22:14 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/30 02:10:54 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_select.h"

static void			space_padding(t_select *choices, int len)
{
	t_select		*tmp;
	int				nspaces;

	tmp = choices;
	while (tmp)
	{
		nspaces = len - ft_strlen(tmp->choice);
		tmp->padding = (char *)malloc(sizeof(char) * (nspaces + 1));
		ft_memset(tmp->padding, ' ', nspaces);
		tmp->padding[nspaces] = '\0';
		tmp = tmp->next;
	}
}

static void			pars_print(t_select *tmp, t_all *lists)
{
	if (tmp->cursor == 1)
		ft_putstr_fd("\033[4m", lists->fd);
	if (tmp->select == 1)
		ft_putstr_fd("\033[7m", lists->fd);
	ft_putstr_fd(tmp->choice, lists->fd);
	ft_putstr_fd("\033[0m", lists->fd);
	ft_putstr_fd(tmp->padding, lists->fd);
}

static void			print_choices(t_all *lists, int num, int co)
{
	t_select		*tmp;
	int				i;
	int				j;

	i = 0;
	j = 0;
	tmp = lists->choices;
	while (tmp)
	{
		j = j + ft_strlen(tmp->choice);
		if (i >= num || j >= co)
		{
			ft_putchar_fd('\n', lists->fd);
			i = 0;
			j = 0;
		}
		pars_print(tmp, lists);
		free(tmp->padding);
		i++;
		tmp = tmp->next;
	}
	ft_putchar('\n');
}

void				display_choices(t_all *lists, int len)
{
	int				num;
	struct winsize	w;

	tputs(tgetstr("cl", NULL), lists->fd, ft_puts);
	tputs(tgetstr("ho", NULL), lists->fd, ft_puts);
	tputs(tgetstr("vi", NULL), lists->fd, ft_puts);
	ioctl(lists->fd, TIOCGWINSZ, &w);
	space_padding(lists->choices, len);
	lists->col = w.ws_col / len;
	print_choices(lists, lists->col, w.ws_col);
}
