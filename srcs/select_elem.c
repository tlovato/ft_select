/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   select_elem.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/29 08:44:00 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/29 08:44:10 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_select.h"

static t_return		*get_return(char *choice, t_return **ret)
{
	t_return		*tmp;
	t_return		*new;

	tmp = *ret;
	if ((new = (t_return *)malloc(sizeof(t_return))) == NULL)
		return (NULL);
	new->choice = ft_strdup(choice);
	new->next = NULL;
	if (!tmp)
		*ret = new;
	else
	{
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
	}
	return (*ret);
}

static void			ft_deselect(t_select *tmp, t_return **new)
{
	t_return		*tmpret;
	t_return		*cur;

	cur = *new;
	tmpret = *new;
	while (tmpret && ft_strcmp(tmpret->choice, tmp->choice))
	{
		cur = tmpret;
		tmpret = tmpret->next;
	}
	if (cur == tmpret)
		*new = (*new)->next;
	else if (tmpret)
		cur->next = tmpret->next;
	free(tmpret->choice);
	free(tmpret);
	tmpret = NULL;
}

static t_return		*ft_select(t_select **tmp, t_select **start, t_return **new)
{
	(*tmp)->select = 1;
	(*tmp)->cursor = 0;
	if ((*tmp)->next)
		(*tmp)->next->cursor = 1;
	else
		(*start)->cursor = 1;
	return (*new = get_return((*tmp)->choice, new));
}

t_return			*select_elem(t_select *choices, unsigned int read, t_return *new)
{
	t_select		*tmp;
	t_select		*start;

	start = choices;
	if (read == 32)
	{
		tmp = choices;
		while (tmp)
		{
			if (tmp->cursor)
			{
				if (tmp->select)
				{
					tmp->select = 0;
					ft_deselect(tmp, &new);
				}
				else
					new = ft_select(&tmp, &start, &new);
				return (new);
			}
			tmp = tmp->next;
		}
	}
	return (new);
}
