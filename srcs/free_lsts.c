/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_lsts.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/29 14:38:32 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/29 14:38:40 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_select.h"

t_select		*free_choices(t_select *choices)
{
	t_select	*tmpnxt;

	while (choices)
	{
		tmpnxt = choices->next;
		if (choices->choice)
			free(choices->choice);
		free(choices);
		choices = tmpnxt;
	}
	return (NULL);
}

t_return		*free_ret(t_return *ret)
{
	t_return	*tmpnxt;

	while (ret)
	{
		tmpnxt = ret->next;
		if (ret->choice)
			free(ret->choice);
		free(ret);
		ret = tmpnxt;
	}
	return (NULL);
}
