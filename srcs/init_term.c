/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_term.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/27 08:16:00 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/27 08:16:10 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_select.h"

int			initerm(struct termios *term, struct termios *init, t_all *lists)
{
	char	*res;

	if (tgetent(NULL, getenv("TERM")) == ERR)
		return (0);
	if (tcgetattr(lists->fd, term) == ERR || tcgetattr(lists->fd, init) == ERR)
		return (0);
	term->c_lflag &= ~(ICANON);
	term->c_lflag &= ~(ECHO);
	term->c_cc[VMIN] = 1;
	term->c_cc[VTIME] = 0;
	tcsetattr(lists->fd, TCSANOW, term);
	if ((res = tgetstr("cl", NULL)) == NULL)
		return (0);
	tputs(res, lists->fd, ft_puts);
	return (1);
}

int			init_all(t_all *lists, char **av, int i, int cur)
{
	lists->choices = get_list(av, i, cur);
	lists->ret = NULL;
	lists->col = 0;
	lists->enter = 0;
	lists->tmpfd = ttyname(STDIN_FILENO);
	if ((lists->fd = open(lists->tmpfd, O_WRONLY)) < 0)
		return (0);
	return (1);
}
