/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/19 14:05:32 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/30 02:11:25 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_select.h"

int					ft_puts(int c)
{
	ft_putchar_fd(c, 2);
	return (0);
}

static int			longer_name(t_select *choices)
{
	t_select		*tmp;
	int				max;
	int				len;

	max = 0;
	tmp = choices;
	while (tmp)
	{
		len = ft_strlen(tmp->choice);
		if (len > max)
			max = len;
		tmp = tmp->next;
	}
	return (max);
}

static t_return		*ft_select(t_all *lists, struct termios *term)
{
	int				esc;
	int				*buf[5];
	t_return		*new;

	esc = 1;
	new = lists->ret;
	while (esc && lists->choices)
	{
		ft_bzero(buf, 5);
		handle_signals();
		display_choices(lists, lists->len);
		read(0, buf, 4);
		if (!move_cursor(lists, *(unsigned int *)buf))
			esc = 0;
		new = select_elem(lists->choices, *(unsigned int *)buf, new);
		new = suppr_elem(&lists->choices, *(unsigned int *)buf, new);
	}
	return (new);
}

int					main(int ac, char **av)
{
	if (!init_all(&lists, av, 1, 1)
		|| !initerm(&lists.term, &lists.init, &lists))
		return(-1);
	lists.len = longer_name(lists.choices) + 1;
	lists.ret = ft_select(&lists, &lists.term);
	endterm(&lists.term, &lists.init, &lists);
	close(lists.fd);
	if (lists.enter)
		display_return(lists.ret);
	free_all(&lists);
	return (0);
}
