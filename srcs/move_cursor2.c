/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_cursor2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/30 17:19:23 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/30 17:19:32 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_select.h"

static t_select		*go_down(t_select *tmp, int num)
{
	int				i;

	i = 0;
	while (i < num)
	{
		if (!tmp->next)
			return (NULL);
		tmp = tmp->next;
		i++;
	}
	return (tmp);
}

void				move_down(t_select *tmp, t_all *lists)
{
	t_select		*start;
	t_select		*cur;

	start = tmp;
	while (tmp)
	{
		if (tmp->cursor)
		{
			if ((cur = go_down(tmp, lists->col)))
			{
				cur->cursor = 1;
				tmp->cursor = 0;
			}
			return ;
		}
		if (tmp->next)
			tmp = tmp->next;
	}
}

static t_select		*is_cursor(t_select *tmp, int len)
{
	int				i;
	t_select		*ret;

	while (tmp)
	{
		ret = tmp;
		i = 0;
		while (i < len && ret)
		{
			if (!ret->next)
				return (NULL);
			ret = ret->next;
			i++;
		}
		if (ret && ret->cursor)
			return (tmp);
		tmp = tmp->next;
	}
	return (NULL);
}

void				move_up(t_select *tmp, t_all *lists)
{
	t_select		*start;
	t_select		*cur;

	start = tmp;
	while (tmp)
	{
		if (tmp->cursor)
		{
			if ((cur = is_cursor(start, lists->col)))
			{
				cur->cursor = 1;
				tmp->cursor = 0;
			}
			return ;
		}
		if (tmp->next)
			tmp = tmp->next;
	}
}
