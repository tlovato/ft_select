/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signals.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/08 12:41:42 by tlovato           #+#    #+#             */
/*   Updated: 2016/10/08 12:41:53 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/ft_select.h"

static void		handle_resize(void)
{
	tputs(tgetstr("cl", NULL), lists.fd, ft_puts);
	display_choices(&lists, lists.len);
}

static void		handle_stop(void)
{
	char		cmd[2];

	cmd[0] = 26;
	cmd[1] = 0;
	lists.term.c_lflag |= (ICANON);
 	lists.term.c_lflag |= (ECHO);
 	signal(SIGTSTP, SIG_DFL);
 	tcsetattr(lists.fd, TCSANOW, &lists.init);
 	tputs(tgetstr("te", NULL), lists.fd, ft_puts);
 	tputs(tgetstr("ve", NULL), lists.fd, ft_puts);
 	ioctl(0, TIOCSTI, cmd);
 	tputs(tgetstr("cl", NULL), lists.fd, ft_puts);
}


static void		handle_continue(void)
{
	initerm(&lists.term, &lists.init, &lists);
	display_choices(&lists, lists.len);
}

static void		pars_signal(int sig)
{
	if (sig == SIGWINCH)
		handle_resize();
	else if (sig == SIGTSTP)
		handle_stop();
	else if (sig == SIGCONT)
		handle_continue();
}

void			handle_signals(void)
{
	int			sig;

	sig = 0;
	while (sig < 32)
	{
		signal(sig, &pars_signal);
		sig++;
	}
}
