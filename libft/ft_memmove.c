/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 10:59:43 by tlovato           #+#    #+#             */
/*   Updated: 2015/12/15 19:40:28 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	size_t		i;

	if (dest == src)
		return (dest);
	if (dest > src)
	{
		while (n)
		{
			n--;
			((unsigned char *)dest)[n] = ((unsigned char*)src)[n];
		}
		return (dest);
	}
	i = 0;
	while (i != n)
	{
		((unsigned char*)dest)[i] = ((unsigned char *)src)[i];
		i++;
	}
	return (dest);
}
