/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 09:56:13 by tlovato           #+#    #+#             */
/*   Updated: 2015/12/15 19:51:59 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *dest, const char *src, size_t size)
{
	size_t		destlen;

	destlen = ft_strlen(dest);
	if (size <= destlen)
		return (size + ft_strlen(src));
	ft_strncat(dest, src, size - destlen - 1);
	return (destlen + ft_strlen(src));
}
