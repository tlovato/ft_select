# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tlovato <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/09/19 14:01:34 by tlovato           #+#    #+#              #
#    Updated: 2016/09/19 14:01:36 by tlovato          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_select
INC_DIR = incs
SRCS_DIR = srcs/

SRCS_FILES = main.c init_term.c display.c linked_list.c ending_term.c move_cursor.c\
select_elem.c free_lsts.c move_cursor2.c display_return.c suppr_elem.c signals.c

SRCS = $(addprefix $(SRCS_DIR), $(SRCS_FILES))

OBJS = $(SRCS:.c=.o)

all: $(NAME)

%.o: %.c
		gcc -g -o $@ -c $<

$(NAME): $(OBJS)
			@echo "\033[1;32m Objects O.K.\033[m"
			@make -C libft/
			@gcc -o $@ $^ -L libft/ -lft -g3 -lncurses -Wall -Wextra -Werror
			@echo "\033[1;32m Minishell O.K.\033[m"

clean:
			@rm -f $(OBJS)
			@echo "\033[1;31m Cleaning O.K.\033"

fclean: clean
			@rm -f $(NAME)
			@cd libft && make fclean
			@echo "\033[1;31m Fcleaning O.K.\033[m"

re: fclean all

.PHONY: all clean fclean re
