/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/19 14:01:51 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/19 14:01:52 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SELECT_H
# define FT_SELECT_H

# include "libft.h"
# include "libftprintf.h"
# include <termios.h>
# include <unistd.h>
# include <term.h>
# include <curses.h>
# include <sys/ioctl.h>
# include <signal.h>

typedef struct			s_select
{
	char				*choice;
	char				*padding;
	int					cursor;
	int					select;
	struct s_select		*next;
}						t_select;

typedef struct			s_return
{
	char				*choice;
	struct s_return		*next;
}						t_return;

typedef struct			s_all
{
	struct s_select		*choices;
	struct s_return		*ret;
	int					col;
	int					enter;
	char				*tmpfd;
	int					fd;
	int					len;
	struct termios		term;
	struct termios		init;
	int					stop;
}						t_all;

t_all					lists;

int						ft_puts(int c);
int						initerm(struct termios *t, struct termios *i, t_all *l);
int						init_all(t_all *lists, char **av, int i, int cur);
int						escape(char read);
void					endterm(struct termios *t, struct termios *i, t_all *l);
void					free_all(t_all *lists);
t_select				*get_list(char **av, int i, int cur);
t_select				*free_choices(t_select *choices);
t_return				*free_ret(t_return *ret);
void					display_choices(t_all *lists, int len);
int						move_cursor(t_all *lists, unsigned int read);
void					move_down(t_select *tmp, t_all *lists);
void					move_up(t_select *tmp, t_all *lists);
t_return				*select_elem(t_select *c, unsigned int r, t_return *rt);
void					display_return(t_return *ret);
t_return 			   	*suppr_elem(t_select **c, unsigned int r, t_return *n);
void					handle_signals(void);

#endif
